import static org.junit.jupiter.api.Assertions.*;

class CarroTest {

    @org.junit.jupiter.api.Test
    public void obtenerBalance() {
        Carro carro = new Carro();
        Producto producto1 = new Producto("001", "producto1", "algo", 19.99);
        Producto producto2 = new Producto("002", "producto2", "algo", 39.99);

        carro.agregarItem(producto1);
        carro.agregarItem(producto2);

        assertEquals(59.98, carro.obtenerBalance(), 0.01);

    }
    @org.junit.jupiter.api.Test
    public void obtenerCantidadItem() {
        Carro carro = new Carro();
        Producto producto1 = new Producto("001", "producto1", "algo", 19.99);
        Producto producto2 = new Producto("002", "producto2", "algo", 39.99);

        carro.agregarItem(producto1);
        carro.agregarItem(producto2);

        assertEquals(2, carro.obtenerCantidadItem());

    }
}