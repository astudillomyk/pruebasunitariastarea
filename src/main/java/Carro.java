import java.util.ArrayList;
import java.util.List;

public class Carro {
    private List<Producto> items;

    public Carro() {
        this.items = new ArrayList<>();
    }

    public double obtenerBalance() {
        double balance = 0;
        for (Producto producto : items) {
            balance += producto.getPrecio();
        }
        return balance;
    }

    public void agregarItem(Producto producto) {
        items.add(producto);
    }

    public int obtenerCantidadItem() {
        return items.size();
    }

    public void vaciar() {
        items.clear();
    }
}

